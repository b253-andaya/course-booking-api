const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
  // The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
  // The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

// Route for user registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

// ========================= ACTIVITY ========================
// router.post("/details", (req, res) => {
//   userController
//     .getProfile(req.body)
//     .then((resultFromController) => res.send(resultFromController))
//     .catch((err) => res.send(err));
// });

// Route for retrieving user details
// router.post("/details", (req, res) => {

// 	// Provides the user's ID for the getProfile controller method
// 	userController.getProfile({ userId : req.body.id }).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

// });

// ===================== LECTURE =========================
router.get("/details", (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

// Route to enroll user to a course
// router.post("/enroll", (req, res) => {
//   let data = {
//     userId: req.body.userId,
//     courseId: req.body.courseId,
//   };

//   userController
//     .enroll(data)
//     .then((resultFromController) => res.send(resultFromController))
//     .catch((err) => {
//       console.error(err);
//       res.status(500).send({ error: `Internal server error` });
//     });
// });

// =========== ACTIVITY S40 ==================
// Route to enroll user authentication to a course
// router.post("/enroll", auth.verify, (req, res) => {
//   try {
//     const userData = auth.decode(req.headers.authorization);
//     const courseId = req.body.courseId;
//     console.log(userData);

//     if (userData.isAdmin)
//       return res.send({
//         error: `Admin users are not allowed to enroll in this course.`,
//       });

//     userController
//       .enroll(courseId, userData)
//       .then((resultFromController) => res.send(resultFromController))
//       .catch((err) => {
//         console.error(err);
//         res.status(500).send({ error: `Internal server error` });
//       });
//   } catch (err) {
//     console.error(err);
//     res.status(400).send({ error: `Error decoding authorization header` });
//   }
// });

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let data = {
    userId: userData.id,
    isAdmin: userData.isAdmin,
    courseId: req.body.courseId,
  };

  if (!data.isAdmin) {
    userController
      .enroll(data)
      .then((resultFromController) => res.send(resultFromController))
      .catch((err) => res.send(err));
  } else {
    res.send(false);
  }
});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
